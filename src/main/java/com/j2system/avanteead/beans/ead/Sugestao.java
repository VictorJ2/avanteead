package com.j2system.avanteead.beans.ead;

import com.j2system.avanteead.entity.MailJava;
import com.j2system.avanteead.util.UtilEmail;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.mail.MessagingException;
import org.primefaces.context.RequestContext;

@ViewScoped
@Named("sugestao")
public class Sugestao implements Serializable {

    private String nomeEmpresa;
    private String tipoSugestao;
    private String textoSugestao;

    public Sugestao() {
        this.nomeEmpresa = "";
        this.tipoSugestao = "";
        this.textoSugestao = "";
    }

    public void gravar() throws MessagingException {
        List<String> emails = new ArrayList();
        emails.add("contato@avantetech.com.br");

        enviarEmail(emails);
    }

    public String enviarEmail(List<String> emails) throws MessagingException {

        String assunto = this.tipoSugestao;
        this.textoSugestao = this.textoSugestao.replaceAll("\n", "<br/>");
        String conteudo = "<html><body><center>Sugestão para o AvanteWeb</center><br/><br/><strong>Empresa: </strong>";
        conteudo += this.nomeEmpresa + "<br/>";
        conteudo += "<strong>Sugestão: </strong>";
        conteudo += this.textoSugestao;
        conteudo += "</body></html>";

        MailJava mj = new MailJava();
        //configuracoes de envio
        mj.setSmtpHostMail("smtp.gmail.com");
        mj.setSmtpPortMail("Com SSL");
        mj.setSmtpAuth("true");
        mj.setSmtpStarttls("true");
        mj.setUserMail("solicitacao.avanteweb@gmail.com");
        mj.setFromNameMail("SUGESTAO - AVANTEWEB");
        mj.setPassMail("Avante@2019");
        mj.setCharsetMail("ISO-8859-1");
        mj.setSubjectMail(assunto);
        mj.setBodyMail(conteudo);
        mj.setTypeTextMail(MailJava.TYPE_TEXT_HTML);
        
        //sete quantos destinatarios desejar
        Map<String, String> map = new HashMap<>();

        for (int i = 0; i < emails.size(); i++) {
            map.put(emails.get(i), "");
        }

        mj.setToMailsUsers(map);

        try {
            UtilEmail.senderMail(mj);
            RequestContext.getCurrentInstance().execute("PF('sucessoEnvio').show();"); 
            return "Sucesso";
        } catch (UnsupportedEncodingException | MessagingException | SecurityException e) {
            return "Ocorreu um erro ao enviar email!";
        }
    }
    
    public void fechaDialogSucesso(){
        RequestContext.getCurrentInstance().execute("PF('sucessoEnvio').hide();"); 
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getTipoSugestao() {
        return tipoSugestao;
    }

    public void setTipoSugestao(String tipoSugestao) {
        this.tipoSugestao = tipoSugestao;
    }

    public String getTextoSugestao() {
        return textoSugestao;
    }

    public void setTextoSugestao(String textoSugestao) {
        this.textoSugestao = textoSugestao;
    }

}
