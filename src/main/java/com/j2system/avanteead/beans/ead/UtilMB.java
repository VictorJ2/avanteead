
package com.j2system.avanteead.beans.ead;

import com.j2system.avanteead.util.UtilRequest;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@ViewScoped
@Named("utilMB")
public class UtilMB implements Serializable{

    private BigDecimal idEmpresa;
    private boolean permissao;
    
    public UtilMB() {
        idEmpresa = (BigDecimal) UtilRequest.obterSessao().getAttribute("idEmpresa");
        if(idEmpresa != null){
            permissao = true;
        }
    }
    
    public void verificaAcesso(){
        if(!permissao){
            //Envia pra tela de permissão de acesso negada
        }
    }
    
}
