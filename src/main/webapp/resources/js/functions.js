//FUNÇÕES JAVASCRIPT PARA TECLAS DE ATALHO DA TELA DE LOGIN

//Função para mudar foco do Login para Senha
function funcaoFocarSenhaLogin(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            document.getElementById("formLogin:password").focus();
            document.getElementById("formLogin:password").select();
        }
    } else {
        if (tecla.which === 13) {
            document.getElementById("formLogin:password").focus();
            document.getElementById("formLogin:password").select();
        }
    }
}

//Função para validar a senha e entrar no sistema
function funcaoValidaSenha(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            validaSenha();
            document.getElementById("formLogin:username").focus();
            document.getElementById("formLogin:username").select();
        }
    } else {
        if (tecla.which === 13) {
            validaSenha();
            document.getElementById("formLogin:username").focus();
            document.getElementById("formLogin:username").select();
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------------------------------//

//FUNÇÕES JAVASCRIPT PARA CONTROLE DE TELAS E TECLAS DE ATALHO NA TELA DE EMISSÃO DE NFCE

//Faz a consulta e inserção do produto pelo campo Código
function funcaoSelecionaProduto(tecla) {
    //alert(window.event.keyCode);
    if (window.event) {
        if (tecla.keyCode === 13) {
            selecionaProduto();
        }
    } else {
        if (tecla.which === 13) {
            selecionaProduto();
        }
    }
}

//Habilita e foca o campo Quantidade
function funcaoAlteraQuantidade(tecla) {
    if (window.event) {
        if (tecla.keyCode === 113) {
            alteraQuantidade();
        }
    } else {
        if (tecla.which === 113) {
            alteraQuantidade();
        }
    }
}

//Sai e desabilita campo Quantidade e foca campo Código
function funcaoDesabilitaQuantidade(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            desabilitaQuantidade();
        }
    } else {
        if (tecla.which === 13) {
            desabilitaQuantidade();
        }
    }
}

//Abre o dialog de consulta de produtos
function funcaoAbreDialogConsultaProduto(tecla) {
    if (window.event) {
        if (tecla.keyCode === 115) {
            PF('produtoDialog').show();
        }
    } else {
        if (tecla.which === 115) {
            PF('produtoDialog').show();
        }
    }
}

//Abre o dialog de pagamentos
function funcaoEfetuaPagamento(tecla) {
    if (window.event) {
        if (tecla.keyCode === 118) {
            abrirDialogPagamento();
        }
    } else {
        if (tecla.which === 118) {
            abrirDialogPagamento();
        }
    }
}

//Função chamada a partir do Managed Bean para abrir dialog de pagamentos
function funcaoAbrirDialogPagamento() {
    PF('pgtoDialog').show();
}

//Função chamada a partir do Managed Bean para abrir dialog de finalização de venda
function funcaoAbrirDialogFinalizaVenda() {
    PF('pgtoDialog').hide();
    PF('finalizaDialog').show();
}

//Fecha janela de venda
function funcaoSair(tecla) {
    if (window.event) {
        if (tecla.keyCode === 27) {
            sair();
        }
    } else {
        if (tecla.which === 27) {
            sair();
        }
    }
}

//Consulta produto pelo código no dialog de consulta
function funcaoConsultaProdutoCodigo(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            consultaProdutoCodigo();
            document.getElementById("formNFCe:consultaCodigoProduto").focus();
            document.getElementById("formNFCe:consultaCodigoProduto").select();
        }
    } else {
        if (tecla.which === 13) {
            consultaProdutoCodigo();
            document.getElementById("formNFCe:consultaCodigoProduto").focus();
            document.getElementById("formNFCe:consultaCodigoProduto").select();
        }
    }
}

//Consulta produto pela descrição no dialog de consulta
function funcaoConsultaProdutoDescricao(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            consultaProdutoDescricao();
        }
    } else {
        if (tecla.which === 13) {
            consultaProdutoDescricao();
        }
    }
}

//Função chamada no evento oncomplete do remoteCommand consultaProduto para focar campo quantidade da consulta
//NÃO ESTÁ SENDO UTILIZADA
function funcaoFocarQuantidadeConsulta() {
    document.getElementById("formNFCe:consultaQuantidade").focus();
    document.getElementById("formNFCe:consultaQuantidade").select();
}

//Função chamada no evento oncomplete do remoteCommand alteraQuantidade
function funcaoFocarQuantidade() {
    document.getElementById("formNFCe:quantidade").focus();
    document.getElementById("formNFCe:quantidade").select();
}

//Função chamada a partir do Managed Bean ao remover produto para focar campo Código
function funcaoFocarCodigoProduto() {
    document.getElementById("formNFCe:codigoProduto").focus();
    document.getElementById("formNFCe:codigoProduto").select();
}

//Limpa a tela para iniciar nova venda
function funcaoCancelarVenda(tecla) {
    if (window.event) {
        if (tecla.keyCode === 120) {
            if (confirm('Confirma o cancelamento da venda?')) {
                cancelaVenda();
            }
        }
    } else {
        if (tecla.which === 120) {
            if (confirm('Confirma o cancelamento da venda?')) {
                cancelaVenda();
            }
        }
    }
}

//Função para fechar e limpar dialog de consulta de produtos
function funcaoFecharDialogConsultaProduto(tecla) {
    if (window.event) {
        if (tecla.keyCode === 27) {
            limpaConsulta();
            PF('produtoDialog').hide();
            document.getElementById("formNFCe:codigoProduto").focus();
        }
    } else {
        if (tecla.which === 27) {
            limpaConsulta();
            PF('produtoDialog').hide();
            document.getElementById("formNFCe:codigoProduto").focus();
        }
    }
}

//Função para inserir o produto consultado na tabela de produtos
function funcaoInserirProdutoConsulta(tecla) {
    if (window.event) {
        if (tecla.keyCode === 113) {
            inserirProduto();
            PF('produtoDialog').hide();
            document.getElementById("formNFCe:codigoProduto").focus();
        }
    } else {
        if (tecla.which === 113) {
            inserirProduto();
            PF('produtoDialog').hide();
            document.getElementById("formNFCe:codigoProduto").focus();
        }
    }
}

//Função para adicionar o produto da Tabela de Preços de Produto e fechar o dialog
function funcaoAdicionaProdutoTabelaPreco(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            inserirProdutoTabelaPreco();
            PF('tabelaPrecoProdutoDialog').hide();
            document.getElementById("formNFCe:codigoProduto").focus();
        }
        if (tecla.which === 27) {
            limpaProdutoTabelaPreco();
            PF('tabelaPrecoProdutoDialog').hide();
            document.getElementById("formNFCe:codigoProduto").focus();
        }
    } else {
        if (tecla.which === 13) {
            inserirProdutoTabelaPreco();
            PF('tabelaPrecoProdutoDialog').hide();
            document.getElementById("formNFCe:codifuncaoAdicionaProdutoTabelaPrecoClickgoProduto").focus();
        }
        if (tecla.which === 27) {
            limpaProdutoTabelaPreco();
            PF('tabelaPrecoProdutoDialog').hide();
            document.getElementById("formNFCe:codigoProduto").focus();
        }
    }
}

function funcaoFocarValorPagoPagamento() {
    document.getElementById("formNFCe:valorPago").focus();
    document.getElementById("formNFCe:valorPago").select();
}

//Função para mudar foco do Valor Pago para Tipo Pagamento
function funcaoFocarTipoPagamento(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            document.getElementById("formNFCe:tipoPagto").focus();
        }
    } else {
        if (tecla.which === 13) {
            document.getElementById("formNFCe:tipoPagto").focus();
        }
    }
}

//Função para mudar foco do Acréscimo para Valor Pago
function funcaoFocarValorPago() {
    document.getElementById("formNFCe:valorPago").focus();
    document.getElementById("formNFCe:valorPago").select();
}

//Função para fechar e limpar dialog de pagamentos
function funcaoFecharDialogPagamento(tecla) {
    if (window.event) {
        if (tecla.keyCode === 27) {
            limpaPagamento();
            PF('pgtoDialog').hide();
            document.getElementById("formNFCe:codigoProduto").focus();
        }
    } else {
        if (tecla.which === 27) {
            limpaPagamento();
            PF('pgtoDialog').hide();
            document.getElementById("formNFCe:codigoProduto").focus();
        }
    }
}

//Função para adicionar o pagamento na tabela
function funcaoInserirPagamento(tecla) {
    if (window.event) {
        if (tecla.keyCode === 113) {
            inserirPagamento();
        }
    } else {
        if (tecla.which === 113) {
            inserirPagamento();
        }
    }
}

//Função para fechar dialog de consulta de vendas
function funcaoFecharConsultaVenda(tecla) {
    if (window.event) {
        if (tecla.keyCode === 27) {
            PF('consultaVendaDialog').hide();
        }
    } else {
        if (tecla.which === 27) {
            PF('consultaVendaDialog').hide();
        }
    }
}

//Função para acessar diretamente o dialog de finalização da venda
function funcaoFecharPagamentoAbrirFinalizacao(tecla) {
    if (window.event) {
        if (tecla.keyCode === 119) {
            verificaPagamentoTotalInserido();
            abrirDialogFinalizaVenda();
        }
    } else {
        if (tecla.which === 119) {
            verificaPagamentoTotalInserido();
            abrirDialogFinalizaVenda();
        }
    }
}

function funcaoInserirPagamento(tecla) {
    if (window.event) {
        if (tecla.keyCode === 113) { 
            inserirPagamento();
        }
    } else {
        if (tecla.which === 113) {
            inserirPagamento();
        }
    }
}

//Função para consultar o participante pelo código
function funcaoConsultaParticipantePorCodigo(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            selecionaParticipantePorCodigo();
        }
    } else {
        if (tecla.which === 13) {
            selecionaParticipantePorCodigo();
        }
    }
}

//Função para consultar o participante pelo Nome
function funcaoConsultaParticipantePorNome(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            selecionaParticipantePorNome();
        }
    } else {
        if (tecla.which === 13) {
            selecionaParticipantePorNome();
        }
    }
}

//Função para fechar dialog de finalização de venda
function funcaoFecharDialogFinalizacao(tecla) {
    if (window.event) {
        if (tecla.keyCode === 27) {
            limpaParticipante();
            document.getElementById("formNFCe:codigoProduto").focus();
        }
    } else {
        if (tecla.which === 27) {
            limpaParticipante();
            document.getElementById("formNFCe:codigoProduto").focus();
        }
    }
}

//Função para fechar dialog de finalização de venda de dentro do Managed Bean
function funcaoFechaDialogFimVenda() {
    fecharDialogEspera();
    document.getElementById("formNFCe:codigoProduto").focus();
}

//Função para enviar a venda
function funcaoEnviarVenda(tecla) {
    if (window.event) {
        if (tecla.keyCode === 119) {
            PF('finalizaDialog').hide();
            //abrirDialogEspera();
            enviaVenda();
        }
    } else {
        if (tecla.which === 119) {
            PF('finalizaDialog').hide();
            //abrirDialogEspera();
            enviaVenda();
        }
    }
}

//Função para aplicar desconto no item da venda
function funcaoAplicarDescontoItem(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            aplicaDescontoItem();
            document.getElementById("formNFCe:codigoProduto").focus();
        }
    } else {
        if (tecla.which === 13) {
            aplicaDescontoItem();
            document.getElementById("formNFCe:codigoProduto").focus();
        }
    }
}

//Função para fechar dialog de desconto no item da venda
function funcaoSairDescontoItem(tecla) {
    if (window.event) {
        if (tecla.keyCode === 27) {
            PF('descontoDialog').hide();
            document.getElementById("formNFCe:codigoProduto").focus();
        }
    } else {
        if (tecla.which === 27) {
            PF('descontoDialog').hide();
            document.getElementById("formNFCe:codigoProduto").focus();
        }
    }
}

//Função para aplicar acréscimo no item da venda
function funcaoAplicarAcrescimoItem(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            aplicaAcrescimoItem();
            document.getElementById("formNFCe:codigoProduto").focus();
        }
    } else {
        if (tecla.which === 13) {
            aplicaAcrescimoItem();
            document.getElementById("formNFCe:codigoProduto").focus();
        }
    }
}

//Função para fechar dialog de acréscimo no item da venda
function funcaoSairAcrescimoItem(tecla) {
    if (window.event) {
        if (tecla.keyCode === 27) {
            PF('acrescimoDialog').hide();
            document.getElementById("formNFCe:codigoProduto").focus();
        }
    } else {
        if (tecla.which === 27) {
            PF('acrescimoDialog').hide();
            document.getElementById("formNFCe:codigoProduto").focus();
        }
    }
}

//Função que mostra o dialog de espera com gif
function abrirDialogEspera() {
    PF('envioNfeDialog').show();
}

//Função que fecha o dialog de espera com gif
function fecharDialogEspera() {
    PF('envioNfeDialog').hide();
}

//Função para focar campo banco quando pagamento com cheque
function funcaoFocarBancoCheque() {
    document.getElementById("formNFCe:bancoCheque").focus();
    document.getElementById("formNFCe:bancoCheque").select();
}

//Função para focar agência do cheque com enter
function funcaoFocarAgenciaCheque(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            document.getElementById("formNFCe:agenciaCheque").focus();
            document.getElementById("formNFCe:agenciaCheque").select();
        }
    } else {
        if (tecla.which === 13) {
            document.getElementById("formNFCe:agenciaCheque").focus();
            document.getElementById("formNFCe:agenciaCheque").select();
        }
    }

}

//Função para focar conta do cheque com enter
function funcaoFocarContaCheque(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            document.getElementById("formNFCe:contaCheque").focus();
            document.getElementById("formNFCe:contaCheque").select();
        }
    } else {
        if (tecla.which === 13) {
            document.getElementById("formNFCe:contaCheque").focus();
            document.getElementById("formNFCe:contaCheque").select();
        }
    }

}

//Função para focar número do cheque com enter
function funcaoFocarNumeroCheque(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            document.getElementById("formNFCe:numeroCheque").focus();
            document.getElementById("formNFCe:numeroCheque").select();
        }
    } else {
        if (tecla.which === 13) {
            document.getElementById("formNFCe:numeroCheque").focus();
            document.getElementById("formNFCe:numeroCheque").select();
        }
    }

}

//Função para focar bom para do cheque com enter
function funcaoFocarBomParaCheque() {
    if (window.event.keyCode === 13) {
        document.getElementById("formNFCe:bomParaCheque").focus();
        document.getElementById("formNFCe:bomParaCheque").select();
    }
}

//----------------------------------------------------------------------------------------------------------------------------------------------//

//FUNÇÕES JAVASCRIPT PARA TABULAÇÃO AUTOMÁTICA COM ENTER

function autoTab(elemento) {
    var keyCode = event.keyCode ? event.keyCode : elemento.which ? elemento.which : event.charCode;
    if (keyCode === 13) {
        var i;
        for (i = 0; i < elemento.form.elements.length; i++)
            if (elemento === elemento.form.elements[i])
                break;
        i = (i + 1) % elemento.form.elements.length;
        elemento.form.elements[i].focus();
        elemento.form.elements[i].select();
        event.preventDefault();
        return false;
    }
    return false;
}

function autoTabBomPara(tecla, elemento) {
    if (window.event) {
        var keyCode = tecla.keyCode;
        if (keyCode === 13) {
            var i;
            for (i = 0; i < elemento.form.elements.length; i++)
                if (elemento === elemento.form.elements[i])
                    break;
            i = (i + 1) % elemento.form.elements.length;
            elemento.form.elements[i].focus();
            elemento.form.elements[i].select();
            event.preventDefault();
            return false;
        }
        return false;
    } else {
        var keyCode = tecla.which;
        if (keyCode === 13) {
            var i;
            for (i = 0; i < elemento.form.elements.length; i++)
                if (elemento === elemento.form.elements[i])
                    break;
            i = (i + 1) % elemento.form.elements.length;
            elemento.form.elements[i].focus();
            elemento.form.elements[i].select();
            event.preventDefault();
            return false;
        }
        return false;
    }

}

//----------------------------------------------------------------------------------------------------------------------------------------------//

//Função para emitir fechamento de caixa
function emiteFechamentoCaixa() {
    if (confirm('Confirma a emiss\u00e3o do relat\u00f3rio de fechamento de caixa?'))
        fechamentoCaixa();
    else
        return false;
}

//----------------------------------------------------------------------------------------------------------------------------------------------//

//FUNÇÕES JAVASCRIPT PARA CONTROLE DE TIMER NA GERÊNCIA DE XML

function iniciarTimerImpChaveNfe() {
    PF('timer').start();
    importarNfePorChave();
}

function pausarTimerImpChaveNfe() {
    PF('timer').pause();
}

function pararTimerImpChaveNfe() {
    PF('timer').stop();
}

function iniciarTimerImpChaveCte() {
    PF('timer').start();
    importarCtePorChave();
}

function pausarTimerImpChaveCte() {
    PF('timer').pause();
}

function pararTimerImpChaveCte() {
    PF('timer').stop();
}

//----------------------------------------------------------------------------------------------------------------------------------------------//

//FUNÇÕES JAVASCRIPT PARA CONTROLE DE INSERÇÃO DE NFE NO MÓDULO DE AUDITORIA

//Função para adicionar o NFe na lista do relatório Diferencial Alíquota
function funcaoInserirNfe(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            inserirNfe();
        }
    } else {
        if (tecla.which === 13) {
            inserirNfe();
        }
    }
}

function funcaoFocarNumeroNfe() {
    document.getElementById("form:numeroNfe").focus();
    document.getElementById("form:numeroNfe").select();
}

function funcaoInserirCte(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            inserirCte();
        }
    } else {
        if (tecla.which === 13) {
            inserirCte();
        }
    }
}

function funcaoFocarNumeroCte() {
    document.getElementById("form:numeroCte").focus();
    document.getElementById("form:numeroCte").select();
}

function filtroNcmDifAliquota(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            filtrarDados();
        }
    } else {
        if (tecla.which === 13) {
            filtrarDados();
        }
    }
}

function filtroNcm(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            filtrarDados();
        }
    } else {
        if (tecla.which === 13) {
            filtrarDados();
        }
    }
}

function filtroSt(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            filtrarDados();
        }
    } else {
        if (tecla.which === 13) {
            filtrarDados();
        }
    }

}

function filtroCestCliente(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            filtrarDados();
        }
    } else {
        if (tecla.which === 13) {
            filtrarDados();
        }
    }
}

function filtroItensCestCliente(tecla) {
    if (window.event) {
        if (tecla.keyCode === 13) {
            filtrarDadosItens();
        }
    } else {
        if (tecla.which === 13) {
            filtrarDadosItens();
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------------------------------//

//FUNÇÕES JAVASCRIPT PARA CADASTRAR SEÇÃO, GRUPO, MARCA, COR, TAMANHO E UNIDADE DE MEDIDA VIA DIALOG NO CADASTRO DE PRODUTO, NATUREZA OPERAÇÃO
//NO CADASTRO DE EMPRESA, GRUPO NO CADASTRO DE PARTICIPANTE, PLANO DE CONTAS, CONTA DE RESULTADO E CENTRO DE CUSTO NO CADASTRO DE CONTA
//E HISTÓRICO PADRÃO

//Função para atualizar componente Seção no cadastro de Produto
function funcaoAtualizaSecaoProduto() {
    atualizaSecaoProduto();
    PF('secaoProdutoDialog').hide();
}

//Função para atualizar componente Grupo no cadastro de Produto
function funcaoAtualizaGrupoProduto() {
    atualizaGrupoProduto();
    PF('grupoProdutoDialog').hide();
}

//Função para atualizar componente Marca no cadastro de Produto
function funcaoAtualizaMarcaProduto() {
    atualizaMarcaProduto();
    PF('marcaProdutoDialog').hide();
}

//Função para atualizar componente Cor no cadastro de Produto
function funcaoAtualizaCorProduto() {
    atualizaCorProduto();
    PF('corDialog').hide();
}

//Função para atualizar componente Tamanho no cadastro de Produto
function funcaoAtualizaTamanhoProduto() {
    atualizaTamanhoProduto();
    PF('tamanhoDialog').hide();
}

//Função para atualizar componente Unidade de Medida no cadastro de Produto
function funcaoAtualizaUnidadeMedida() {
    atualizaUnidadeMedida();
    PF('unidadeMedidaDialog').hide();
}


//Função para atualizar componente Natureza Operação no cadastro de Empresa
function funcaoAtualizaNaturezaOperacao() {
    atualizaNaturezaOperacao();
    PF('naturezaOperacaoDialog').hide();
}

//Função para atualizar componente Grupo no cadastro de Participante
function funcaoAtualizaGrupoParticipante() {
    atualizaGrupoParticipante();
    PF('grupoClienteDialog').hide();
}

//Função para atualizar componente Plano de Contas no cadastro de Conta
function funcaoAtualizaPlanoContas() {
    atualizaPlanoContas();
    PF('planoContasDialog').hide();
}

//Função para atualizar componente Conta de Resultado no cadastro de Conta
function funcaoAtualizaContaResultado() {
    atualizaContaResultado();
    PF('contaResultadoDialog').hide();
}

//Função para atualizar componente Centro de Custo no cadastro de Conta
function funcaoAtualizaCentroCusto() {
    atualizaCentroCusto();
    PF('centroCustoDialog').hide();
}

//Função para atualizar componente Histórico Padrão
function funcaoAtualizaHistoricoPadrao() {
    PF('historicoPadraoDialog').hide();
}

//Função para atualizar componente Bandeira de Cartão
function funcaoAtualizaBandeiraCartao() {
    atualizaBandeiraCartao();
    PF('bandeiraCartaoDialog').hide();
}

//Função para atualizar componente Cliente no cadastro de Orçamento, Pedido de Venda, Ordem de Serviço e NFe
function funcaoAtualizaCliente() {
    atualizaCliente();
    PF('clienteDialog').hide();
}

//Função para atualizar componente Veículo no cadastro de Ordem de Serviço
function funcaoAtualizaVeiculo() {
    atualizaVeiculo();
    PF('veiculoDialog').hide();
}

//----------------------------------------------------------------------------------------------------------------------------------------------//


//Função para mudar foco da data de vencimento para o valor da parcela na renegociação do Financeiro
function funcaoFocarValorParcelaRenegociacao() {
    document.getElementById("form:valorParcela").focus();
    document.getElementById("form:valorParcela").select();
}


//----------------------------------------------------------------------------------------------------------------------------------------------//
//FUNÇÕES JAVASCRIPT PARA CONTROLE DE TELA NO CADASTRO DE NFE DO FATURAMENTO

//Função chamada a partir do Managed Bean para abrir dialog de emails
function funcaoAbrirDialogEmails() {
    PF('emailsDialog').show();
}

//Função chamada a partir do Managed Bean para fechar dialog de emails
function funcaoFecharDialogEmails() {
    PF('emailsDialog').hide();
}

//Função para colocar o foco no campo Código Produto após inserir na tabela
function funcaoFocarCodigoProdutoNfe() {
    document.getElementById("form:tabView:codigoProduto").focus();
    document.getElementById("form:tabView:codigoProduto").select();
}

//Função para colocar o foco no campo Quantidade Volumes após inserir na tabela
function funcaoFocarQtdeVolume() {
    document.getElementById("form:tabView:qtdeVolumes").focus();
    document.getElementById("form:tabView:qtdeVolumes").select();
}

//Função para colocar o foco no campo Número Duplicata após inserir na tabela
function funcaoFocarNumeroDuplicata() {
    document.getElementById("form:tabView:numeroDuplicata").focus();
    document.getElementById("form:tabView:numeroDuplicata").select();
}


//Função para colocar o foco no campo Chave NFe Referenciada após inserir na tabela
function funcaoFocarChaveNfeReferenciada() {
    document.getElementById("form:tabView:chaveNfeReferenciada").focus();
    document.getElementById("form:tabView:chaveNfeReferenciada").select();
}

//Função para colocar o foco no campo Número ECF Referenciado após inserir na tabela
function funcaoFocarCupomReferenciado() {
    document.getElementById("form:tabView:numeroEcf").focus();
    document.getElementById("form:tabView:numeroEcf").select();
}

//Função para atualizar gráfico de resumo de vendas ao entrar na tela
function atualizaGraficoResumoVendas() {
    atualizaGraficoVendas();
}

//Função para atualizar gráfico de resumo de NFes ao entrar na tela
function atualizaGraficoResumoNfes() {
    atualizaGraficoNfes();
}

//Função para atualizar gráfico de resumo de Notas de Entrada ao entrar na tela
function atualizaGraficoResumoNotasEntrada() {
    atualizaGraficoNotasEntrada();
}

//Função para atualizar gráfico de resumo de compras ao entrar na tela
function atualizaGraficoResumoCompras() {
    atualizaGraficoCompras();
}

//Função para colocar o foco no campo Número NFe ao selecionar Nova Consulta Situação NFe
function funcaoFocarNumeroNfeConsultaSituacao() {
    document.getElementById("form:numeroNfe").focus();
}

//Função para atualizar tabela de extrato detalhado ao entrar na tela
function atualizaExtratoDetalhado() {
    atualizaExtratoDetalhado();
}

//----------------------------------------------------------------------------------------------------------------------------------------------//
//FUNÇÕES JAVASCRIPT PARA CONTROLE DE TELA NO CADASTRO DE PEDIDOS E COTAÇÕES DO MÓDULO DE COMPRAS

//Função para colocar o foco no campo Código Produto após inserir na tabela de itens de pedido
function funcaoFocarCodigoProdutoPedido() {
    document.getElementById("form:tabView:codigoProduto").focus();
    document.getElementById("form:tabView:codigoProduto").select();
}

//Função para colocar o foco no campo Código Produto após inserir na tabela de itens de cotação
function funcaoFocarCodigoProdutoCotacao() {
    document.getElementById("form:tabView:codigoProduto").focus();
    document.getElementById("form:tabView:codigoProduto").select();
}

//Função para colocar o foco no campo Código Produto após alterar na tabela de itens de cotação
function funcaoFocarCodigoProdutoCotacaoAlter() {
    document.getElementById("form:tabView:codigoProduto").focus();
    document.getElementById("form:tabView:codigoProduto").select();
}

//----------------------------------------------------------------------------------------------------------------------------------------------//
//FUNÇÕES JAVASCRIPT PARA CONTROLE DE TELA NA ENTRADA DE NOTA DO COMPRAS

//Função para colocar o foco no campo Código Produto após inserir na tabela
function funcaoFocarCodigoProdutoNota() {
    document.getElementById("form:tabView:codigoProduto").focus();
    document.getElementById("form:tabView:codigoProduto").select();
}

function atualizaGraficoResumoGeral() {
    atualizaGraficoMensal();
}

function atualizaGraficosConciliacaoCartao() {
    atualizaGraficosConciliacaoCartao();
}

function atualizaCamposParticipante() {
    atualizaCamposParticipante();
}