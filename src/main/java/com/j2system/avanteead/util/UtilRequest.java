package com.j2system.avanteead.util;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class UtilRequest {
           
    public static HttpSession obterSessao(){
        HttpServletRequest rq = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession sessao = rq.getSession(true);
        return sessao;
    }       
    
    public static void excluirManagedBeanSessao(String managedBean){
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(managedBean); 
    }
}
